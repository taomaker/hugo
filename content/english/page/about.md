---
title: About me
comments: false
---

My name is Aaron Zhang. I'm a supply chain professional and a open-source supporter. Also writer, programmer, maker, and avid student of life.

Some fun facts:

- I write poems while sitting to my 3D printer.
- My hometown is 50 miles away from Confucius's birth place.

### A few people that I admire
- [Derek Sivers](https://sivers.org/)
- Ansel Adams
- Deng Xiaoping
- Joseph Needham
- Linus Torvalds

### Contact
I love hearing from people. Please email me at **aaron [at] taomaker [dot] com** and introduce yourself. I love getting to know different people and I reply to all. 
