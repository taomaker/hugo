---
title: A checklist for flowing down technical data completely
date: 2017-03-03T10:25:09-04:00
category: SCM
tag: supply chain management
draft: false
---

In supply chain management, incomplete flow down of technical data causes lots of waste and uncertainties: time wasted communicating to clarify requirements; quality issues due to asymmetric understanding of certain specifications; paying expedition fee to ship overdue products, so on and so on.

Making sure technical data is completely flowed down to suppliers and sub suppliers at the very beginning is critical to reduce waste and uncertainty.

### Technical Data Package

Technical Data Package (TDP) is the good starting point to brainstorm your customized package.

A Technical Data Package is a “collection of data enabling design, production, delivery, and/or maintenance that communicates a customer’s product definition, performance criteria, and method of verification to the sources of the deliverable.” (Source AIA/NAS 3500)

Or for defense projects, “A technical description of an item adequate for supporting an acquisition, production, engineering, and logistics support (e.g., Engineering Data for Provisioning, Training, and Technical Manuals). The description defines the required design configuration for performance requirements, and procedures required to ensure adequacy of item performance. It consists of applicable technical data such as models, drawings, associated lists, specifications, standards, performance requirements, quality assurance provisions, software documentation and packing details.” (Source: MIL-STD-31000)

### My Checklist – Files

#### Item-specific files

2D Drawings
3D models
Material requirement, standard and spec
Post processing requirement, standard and spec
Performance/functional test requirements
Method of verification
Critical charisterstics
Fisrt article inspection and testing requirements
Associated internal documents
Packaging requirement

#### Company-specific files

Quality assurance provisions
Workmanship requirements
Sample Approval form
Deviation request form

#### My checklist – questions to ask

Are drawing and model up to date?
Shall product be made in accordance with 2D drawing, 3D model, or other requirement?
Are the industrial/national standards in TDP up to date?
Is there any tooling information that needs to be passed down?
Is there any test equipment information that needs to flow down?
Is alternative/equivalent material acceptable?
Is alternative/equivalent post processes acceptable?
Is there any process restricted to approved sources?

### Download as a checklist

#### Options

 - Download the newest PDF version of the checklist.
 - Customize from raw file (text/markdown) coming soon

### Open Source

This article and the checklist file is created from crowd wisdom and is shared as public domain work.`
