## 首页 
### 我是谁
一个在波士顿工作的山东人，经历山东-武汉-香港-麻州，从事制造业供应链工作，热爱开源与各种新鲜科技。
### 关于此网站
受到[谢益辉](https://yihui.name/)严重影响。

- 服务器：[GitLab Pages](https://about.gitlab.com/features/pages/)
- 生成器：[Hugo](https://gohugo.io) 
- 主题: [Hugo Ivy](https://github.com/yihui/hugo-ivy)
- 字体：[思源宋体](https://github.com/adobe-fonts/source-han-serif/tree/release/)
