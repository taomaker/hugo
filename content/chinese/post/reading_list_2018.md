---
title: "2018年书单"
slug: "2018-reading-list"
date: 2018-10-17T21:57:38-04:00
draft: false
---

### 中文
- 傅高义 [《邓小平时代》]({{< ref "/book/dengxiaoping" >}})
- 史铁生 《我与地坛》
- 丰子恺 《活着本来单纯》
- 李泽厚 《美的历程》
- 傅佩荣 《国学的天空》
- 吴军 《见识》
- 《时间简史》
- 乔治·奥威尔 《1984》
- 查理·芒格 《穷查理宝典》
- 刘慈欣 《三体》

### 英文
- Cryptoassets
- Ray Dalio, Principles
- Cal Newport, So Good They Can't Ignore You
- Tim Ferriss, Tools of Titans
- Stephen Covey, The 7 Habits of Highly Effective Families
- Victor Frankl, Man's Search for Meaning
- Charles Duhigg, The Power of Habit
- Stephen Cover, The 7 Habits of Highly Effective People
