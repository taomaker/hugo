---
title: "2017年书单"
slug: "2017-reading-list"
date: 2017-01-01T21:57:38-04:00
draft: false
---

### 中文
- 奥野宣之 《如何有效阅读一本书》
- 李瑞环 《学哲学，用哲学》

### 英文
- Jonathan Spencer, The Search for Modern China
- Sonja Lyubomirsky, The How of Happiness
- John Hirst, The Shortest History of Europe
- Guy Kawasaki, The Art of the Start
- Peter Thiel, Zero to One
- Adam Grant, Give and Take
- Benjamin Graham, The Intelligent Investor : The Definitive Book on Value Investing
